# README #

This is my Final Challenge where we were tasked with creating a Proposal App. This app is unique as it relies solely on Fiori Elements in terms of annotations, rather than JS controllers or XML views. 

### Prerequisites ###

After cloning the repository into the folder of your choice, run these commands in order:

* Run cd final-challenge
* Run npm i -g
* Run npm i @sap/cds-dk
* Type cds watch to run the application!

### TO-DOs ###

* Fix association with Components and Employees
* Fix color coding with Uncertainties in Proposal
* Massive clean-up of unecessary code.
* Database configuration
* Fixing XSUAA for deployment into HANA


### Who do I talk to? ###

* Feel free to contact me at nsr@gavdilabs.com for any insights on how the app was created, as well as useful links for what I based some features on.
