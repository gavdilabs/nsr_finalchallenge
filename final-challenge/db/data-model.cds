using {Currency, managed, cuid, sap.common} from '@sap/cds/common';


namespace sap.capire.proposal;

type Url : String;


type TechnicalBooleanFlag : Boolean @(
    UI.Hidden,
    Core.Computed
);

type Criticality : Integer @(
    UI.Hidden,
    Core.Computed
);
type TechnicalFieldControlFlag : Integer @(
    UI.Hidden,
    Core.Computed
);

type Identifier : String(100)@(title : 'Identifier');
@cds.autoexpose
entity identified : cuid {
    identifier : Identifier not null;
}


annotate identified with @(
    Common.SemanticKey : [identifier],
    UI.Identification  : [{Value : identifier}]
) {

    ID         @Common : {
        Text            : identifier,
        TextArrangement : #TextOnly
        
    };
}

entity Proposal : managed {
    key ProposalID :  UUID                           @title: 'Proposal ID';
        identifier : localized String                @title :'Identifier';
        title      :  localized String(111)          @title    : 'Title';
        @assert.integrity : false
        uncertainty: Association to one Uncertainty  @title : 'Uncertainty';
        @assert.integrity : false
        uncertainty_name : Association to one Uncertainty @title : 'Uncertainty';
        descr      :  localized String(500)          @title    : 'Description';
        employee : Association to one Employees;
        employee_name : Association to one Employees;
        assignedEmployee     : Association to one Employees;
        EmployeeList   :    Association to many Employees
                            on EmployeeList.proposal = $self;
        @assert.integrity: false
        component      : Association to one Components @title : 'Component(s)';
        category   : Association to one Category     @title: 'Category';
        price      :  Decimal(9, 2)                  @title    : 'Price' ;
        @assert.integrity : false
        currency   :  Currency                       @title    : 'Currency';

        image      : String @title: 'https://picsum.photos/200';
          isDraft                 : TechnicalBooleanFlag not null default false;
        identifierFieldControl  : TechnicalFieldControlFlag not null default 7; 
        man_hours :  Integer                         @title : 'Estimated Time (hours)';

}


entity Employees : managed {
    key employeeId : UUID @title : 'Employee ID';
    employeeName : String @title : 'Employee Name';
    employeeInit : String @title : 'Employee Initials';
    experience : String @title : 'Experience Level';
    email   :String@title : 'Email Address';
    image      : String @title: 'https://picsum.photos/200';
    proposal    : Association to Proposal;

    @assert.integrity : false
    Proposal : Association to many Proposal
                    on Proposal.assignedEmployee = $self;
    

}



entity Components : managed {
    key ComponentID      : UUID    @title          : 'Component ID';
        ComponentName    : String  @title : 'Component Name';
        uncertainty_code : String  @title : 'Uncertainty Level';


}

entity Estimates : managed {
    key EstimateID : UUID;
        price      : Decimal(9, 2)@title : 'Price';

}

entity CurrencyData : managed {
        code      : String @title :'Currency Code';
        name      : String @title :'Currency Name';
        symbol      : String @title :'Currency Symbol';
        descr      : String @title :'Currency Description ';


}

entity ProposalCodeList : common.CodeList{
    key code :String(20)
}

entity Uncertainty :  ProposalCodeList {
    uncertainty_name : String @title : 'Uncertainty';
  uncertainty_code : Criticality not null default 3 @title : 'Uncertainty';
  uncertainty_descr :  String @title : 'DEscription';
}

entity Category : ProposalCodeList {}

