using ProposalService as my from '../srv/cat-service';
using {Currency, managed, cuid, sap.common} from '@sap/cds/common';
using from '../db/data-model';


//Main Page 


annotate my.Proposal with @(
	Common.SemanticKey: [title, price],
	UI: {

		LineItem: [
			{
				$Type: 'UI.DataField',
				Value: ProposalID
			},
			//{Value: identifier},
			{Value: title},
			{Value: descr},
			{
            $Type                     : 'UI.DataField',
			Label : 'Uncertainty',
            Value                     : uncertainty.name,
            Criticality               : uncertainty.code,
            CriticalityRepresentation : #WithoutIcon,
			            ![@UI.Importance] : #Medium
        	},
			{Value: price },			
            {Value: currency_code},
			{Value: man_hours},
			{Value : category_code},
		],
		SelectionFields: [ title, price, uncertainty_code],
		PresentationVariant  : {
			Text : 'Default',
			SortOrder	: [{Property : title},],
			Visualizations : ['@UI.LineItem']
			
		},
		
	
//Object Page 

    HeaderInfo : {
	
			TypeImageUrl: 'sap-icon://alert',
            TypeName: 'Proposal',
            TypeNamePlural: 'Proposals',
			Title: {Value: title },
            Description: {Value: descr}
        },
		
		HeaderFacets                         : [{
        $Type  : 'UI.ReferenceFacet',
        Target : '@UI.FieldGroup#HeaderGeneralInformation'
    }],

		FieldGroup  #HeaderGeneralInformation : {
			$Type : 'UI.FieldGroupType',
			Data : [
				
				{
					$Type: 'UI.DataField',
					Value : ProposalID
				},
				{
					Value : category_code
				},
				{
					$Type: 'UI.DataField',
					Value: price
				},
				{
					$Type: 'UI.DataField',
					Value: man_hours
				},{
					$Type: 'UI.DataField',
					Value: component_ComponentID
				},
			
				{
					$Type: 'UI.DataField',
					Value: currency_code	
				},
					
				{
                $Type  : 'UI.DataFieldForAnnotation',
                Target : 'assignedEmployee/@Communication.Contact',
                Label  : '{i18n>Assigned Employee(s)}'
				
            }

            
			]
			
		}, 
	   FieldGroup #ProposalDetails : {
        $Type : 'UI.FieldGroupType',
        Data : [
            {
                $Type : 'UI.DataField',
                Value : identifier
            },
            {
                $Type : 'UI.DataField',
                Value : title
            },
            {
            $Type : 'UI.DataField',
            Value : descr
        },
		{
			$Type : 'UI.DataField',
           	Value : uncertainty_code,
		},

       ]
    },
	Facets: [
		{
			$Type: 'UI.CollectionFacet', 
			Label: 'Details', 
			ID	 : 'ProposalOverviewFacet',
			Facets: [
				{
					$Type : 'UI.ReferenceFacet',
					Label : 'Proposal Details',
					ID    : 'ProposalDetailsFacet',
					Target: '@UI.FieldGroup#ProposalDetails'
				},
				{
	            	$Type : 'UI.ReferenceFacet',
					Target : '@UI.FieldGroup#HeaderGeneralInformation',
        	        Label : '{i18n>General Information}',
                	ID : 'GeneralInformationFacet',
            	},
        	]
        },
        {
            $Type         : 'UI.ReferenceFacet',
            Label         : '{i18n>Employees}',
            ID            : 'EmployeeFacet',
            Target        : 'EmployeeList/@UI.LineItem',
            ![@UI.Hidden] : false
        }
		]
});


 
annotate  my.Employees with @(UI: {
	
	
	 HeaderInfo : {
	
            TypeName: 'Employee',
            TypeNamePlural: 'Employees',
			Title: {Value: employeeName },
            Description: {Value: employeeId}
        },

		LineItem:[
			{
				$Type : 'UI.DataField',
				Value: employeeName,
			},
			{
				$Type : 'UI.DataField',
				Value: employeeInit
			},
			{
				$Type : 'UI.DataField',
				Value: experience
			},
			{
				$Type : 'UI.DataField',
				Value: email
			}
		],
	}
);



		
	


//using User from  '@sap/cds/common';
/** 
annotate ProposalService.Employees with @(
	UI: {
  
		
	}
);


////////////////////////////////////////////////////////////////////////////
//
//	Prop Object Page
//
/** 
annotate my.Employees with @(
	UI: {
	  SelectionFields: [  ],
		LineItem: [
			{Value: employeeName},
			{Value: employeeInit, Label:'{i18n>Initials}'},
			{Value: experience},
		//	{Value: currency.symbol, Label:' '},
		]
	},
);


////////////////////////////////////////////////////////////////////////////
//
//	Proposal Object Page


annotate my.Employees with @(
	UI: {
		//Facets: [
	//		{$Type: 'UI.ReferenceFacet', Label: '{i18n>General}', Target: '@UI.FieldGroup#General'},
			//{$Type: 'UI.ReferenceFacet', Label: '{i18n>Translations}', Target:  'texts/@UI.LineItem'},
		//	{$Type: 'UI.ReferenceFacet', Label: '{i18n>Employees}', Target: '@UI.FieldGroup#Employees'},
			//{$Type: 'UI.ReferenceFacet', Label: '{i18n>Admin}', Target: '@UI.FieldGroup#Data'},
	//	],
		FieldGroup#General: {
			Data: [
				{Value: employeeId},
				{Value: employeeName},
			]
		},
		FieldGroup#Details: {
			Data: [
				{Value: employeeName},
			   {
        		Value : employeeId,
        		
   			 }
			]
		}
	}
);



/** 
annotate mainService.Proposal.proposal with @(
	UI: {
		Identification: [{Value:title}],
		SelectionFields: [ locale, title ],
		LineItem: [
			{Value: locale, Label: 'Locale'},
			{Value: title, Label: 'Title'},
			{Value: descr, Label: 'Description'},
		]
	}
);

// Add Value Help for Locales
annotate mainService.Proposal.proposal {
	locale @ValueList:{entity:'Languages',type:#fixed}
}
// In addition we need to expose Languages through mainService as a target for ValueList
using { sap } from '@sap/cds/common';
extend my mainService {
	@readonly entity Languages as projection on sap.common.Languages;
}

*/











/** 
           .               ,.
          T."-._..---.._,-"/|
          l|"-.  _.v._   (" |
          [l /.'_ \; _~"-.`-t
          Y " _(o} _{o)._ ^.|
          j  T  ,--.  T  ]
          \  l ( /-^-\ ) !  !
           \. \.  "~"  ./  /c-..,__
             ^r- .._ .- .-"  `- .  ~"--.
              > \.                      \
              ]   ^.                     \
              3  .  ">            .       Y  -DO YOU ACCEPT THE OFFERING OF THIS KITTY CAT???
 ,.__.--._   _j   \ ~   .         ;       |
(    ~"-._~"^._\   ^.    ^._      I     . l
 "-._ ___ ~"-,_7    .Z-._   7"   Y      ;  \        _
    /"   "~-(r r  _/_--._~-/    /      /,.--^-._   / Y
    "-._    '"~~~>-._~]>--^---./____,.^~        ^.^  !
        ~--._    '   Y---.                        \./
             ~~--._  l_   )                        \
                   ~-._~~~---._,____..---           \
                       ~----"~       \
                                      \


*/





