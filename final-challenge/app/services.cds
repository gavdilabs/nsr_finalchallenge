using from './annotations';
using from './fiori/annotations';

/** 

using ProposalService as service from '../srv/cat-service';
using from '../db/data-model';

annotate service.Proposal with @(
    UI.LineItem : [
        {
            $Type : 'UI.DataField',
            Value : ProposalID,
        },
        {
            $Type : 'UI.DataField',
            Value : title,
        },
        {
            $Type : 'UI.DataField',
            Value : descr,
        },
        {
            $Type : 'UI.DataField',
            Value : price,

        } 
    ],
);

using  from '@sap/cds/common';


annotate service.Employees with@(
    LineItem :[
         {
            $Type : 'UI.DataField',
            Value : employeeId,
        },
        {
            $Type : 'UI.DataField',
            Value : employeeName,
        },
        {
            $Type : 'UI.DataField',
            Value : employeeInit,
        },
        {
            $Type : 'UI.DataField',
            Value : experience,
        } 
    ],
    Facets: [
		//	{$Type: 'UI.ReferenceFacet', Label: '{i18n>Details}', Target: '@UI.Identification'},
		//	{$Type: 'UI.ReferenceFacet', Label: '{i18n>General}', Target: '@UI.FieldGroup#General'},
		{$Type: 'UI.ReferenceFacet', Label: 'Employees', Target:  'LineItem'}
	//		//{$Type: 'UI.ReferenceFacet', Label: '{i18n>Details}', Target: '@UI.FieldGroup#Details'},
	//		//{$Type: 'UI.ReferenceFacet', Label: '{i18n>Admin}', Target: '@UI.FieldGroup#Admin'},
		],
);

*/
