sap.ui.define(['sap/fe/core/AppComponent'], function(AppComponent) {
    'use strict';

    return AppComponent.extend("fiori.Component", {
        metadata: {
            manifest: "json"
        }
    });
});
