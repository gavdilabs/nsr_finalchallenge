
using from '../annotations';
using from '../../../index';
/** 
annotate ProposalService.Proposal with @(
    UI.LineItem : [
        {
            $Type : 'UI.DataField',
            Value : ProposalID,
        },
        {
            $Type : 'UI.DataField',
            Value : title,
        },
        {
            $Type : 'UI.DataField',
            Value : descr,
        },
        {
            $Type : 'UI.DataField',
            Value : price,

        } ,
        {
            Value : image
        }
    ],
);

annotate ProposalService.Employees with @(
    UI.LineItem : [
        {
            $Type : 'UI.DataField',
            Value : employeeId,
        },
        {
            $Type : 'UI.DataField',
            Value : employeeName,
        },
        {
            $Type : 'UI.DataField',
            Value : employeeInit,
        },
        {
            $Type : 'UI.DataField',
            Value : experience,
        },
        {
            Value : image
        }

        
    ],
    FieldGroup  : {
			$Type : 'UI.FieldGroupType',
			Data : [
				{
					$Type :'UI.DataField',
					Value : descr
				}
			]
			
		},
);

/*
  This model controls what gets served to Fiori frontends...
*/


//using from '../../../srv/cat-service';

//using from '../../../db/data-model';
