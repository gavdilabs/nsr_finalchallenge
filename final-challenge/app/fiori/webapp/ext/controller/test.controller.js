sap.ui.define(
    [
        'sap/ui/core/mvc/Controller',
        'sap/fe/core/controllerextensions/RoutingListener'
    ],
    function(Controller, RoutingListener) {
        'use strict';

        return Controller.extend('fiori.ext.test', {
            routingListener: RoutingListener,

            /**
             * Called when a controller is instantiated and its View controls (if available) are already created.
             * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
             * @memberOf fiori.ext.test
             */
            //	onInit: function () {
            //
            //	},

            /**
             * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
             * (NOT before the first rendering! onInit() is used for that one!).
             * @memberOf fiori.ext.test
             */
            //	onBeforeRendering: function() {
            //
            //	},

            /**
             * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
             * This hook is the same one that SAPUI5 controls get after being rendered.
             * @memberOf fiori.ext.test
             */
            //	onAfterRendering: function() {
            //
            //	},

            /**
             * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
             * @memberOf fiori.ext.test
             */
            //	onExit: function() {
            //
            //	}
        });
    }
);
