sap.ui.define(
    [
        'sap/ui/core/mvc/Controller',
        'sap/fe/core/controllerextensions/RoutingListener',
        'sap/ui/model/json/JSONModel',
        'sap/m/MessageToast'
    ],
    function(Controller, RoutingListener, JSONModel, MessageToast) {
        'use strict';

        return Controller.extend("myController", {
            routingListener: RoutingListener,

    constructor: function () {
        Controller.apply(this, arguments);
        // No need to instantiate the extension, it's done automatically
        },

          
        });
    }
);
