//using  mainService  from '@capire/proposal';
//using from '..//common'; // to help UI linter get the complete annotations

////////////////////////////////////////////////////////////////////////////
//
//	Proposal Object Page
//

annotate mainService.Proposal with @(
	UI: {
		Facets: [
			{$Type: 'UI.ReferenceFacet', Label: '{i18n>General}', Target: '@UI.FieldGroup#General'},
			//{$Type: 'UI.ReferenceFacet', Label: '{i18n>Translations}', Target:  'texts/@UI.LineItem'},
			{$Type: 'UI.ReferenceFacet', Label: '{i18n>Details}', Target: '@UI.FieldGroup#Details'},
			{$Type: 'UI.ReferenceFacet', Label: '{i18n>Admin}', Target: '@UI.FieldGroup#Admin'},
		],
		FieldGroup#General: {
			Data: [
				{Value: title},
				{Value: descr},
			]
		},
		FieldGroup#Details: {
			Data: [
				{Value: price},
				{Value: currency_code, Label: '{i18n>Currency}'},
			]
		},
		FieldGroup#Admin: {
			Data: [
				{Value: createdBy},
				{Value: createdAt},
				{Value: modifiedBy},
				{Value: modifiedAt}
			]
		}
	}
);


//annotate sap.capire.proposal.Proposal with @fiori.draft.enabled;

annotate mainService.Proposal.texts with @(
	UI: {
		Identification: [{Value:title}],
		SelectionFields: [ locale, title ],
		LineItem: [
			{Value: locale, Label: 'Locale'},
			{Value: title, Label: 'Title'},
			{Value: descr, Label: 'Description'},
		]
	}
);
/** 
// Add Value Help for Locales
annotate mainService.Proposal.texts {
	locale @ValueList:{entity:'Languages',type:#fixed}
}
// In addition we need to expose Languages through mainService as a target for ValueList
using { sap } from '@sap/cds/common';
extend service mainService {
	@readonly entity Languages as projection on sap.common.Languages;
}
*/