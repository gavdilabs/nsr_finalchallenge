## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Tue Jul 20 2021 13:21:17 GMT-1000 (Hawaii-Aleutian Standard Time)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.2.4|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>List Report Object Page V4|
|**Service Type**<br>Local Cap|
|**Service URL**<br>/proposal/
|**Module Name**<br>fiori|
|**Application Title**<br>Proposal|
|**Namespace**<br>|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Telemetry**<br>True|
|**Main Entity**<br>Proposal|
|**Navigation Entity**<br>texts|

## fiori

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply start your CAP project and navigate to the following location in your browser:

http://localhost:4004/fiori/webapp/index.html

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


