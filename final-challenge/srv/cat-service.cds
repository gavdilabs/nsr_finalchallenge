using  {sap.capire.proposal as my} from '../db/data-model';

//using from '../app/fiori/webapp/services';


service ProposalService 
{
	@odata.draft.enabled
    entity Proposal   as projection on my.Proposal;
	@readonly
    entity Estimates  as projection on my.Estimates;
	@readonly
    entity Components as projection on my.Components;
	@readonly
    entity Employees as projection on my.Employees;
	@readonly 
	entity Category as projection on my.Category;

}
/** 

//Giving Prices to Objects, I have no idea why this needs to be in a seperate annotation. just dont touch it.
annotate my.Proposal with {
		//Common.SemanticKey: [title, price],

		price @title:'Price' @Measures.ISOCurrency: currency_code
};
*/
