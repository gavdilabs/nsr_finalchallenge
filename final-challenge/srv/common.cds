namespace scp.cloud;
using ProposalService as service from './cat-service';

using {
    cuid
} from '@sap/cds/common';

annotate service.Proposal with {
    ID @UI.Hidden: true;
    assignedIndividual @UI.Hidden : true;
    identifier @(Common.FieldControl: identifierFieldControl);
};

annotate service.Proposal with {
    incidentStatus @Common : {
       // Text            : incidentStatus.name,
      //  TextArrangement : #TextOnly,
        ValueListWithFixedValues
    };

 category  @Common : {
        Text            : category.name,
        TextArrangement : #TextOnly,
        //insert your value list here
        ValueList       : {
            $Type          : 'Common.ValueListType',
            Label          : 'Category',
            CollectionPath : 'Category',
            Parameters     : [
            {
                $Type             : 'Common.ValueListParameterInOut',
                LocalDataProperty : category_code,
                ValueListProperty : 'code'
            },
            {
                $Type             : 'Common.ValueListParameterDisplayOnly',
                ValueListProperty : 'descr'
            }
            ]
        }
    };
uncertainty @Common : {
        Text            : uncertainty.name,
        TextArrangement : #TextOnly,
        ValueListWithFixedValues
    };
};



annotate service.Category with {
    code @Common : {
        Text            : name,
        TextArrangement : #TextOnly
    }    @title :  'Category'
};

annotate service.Uncertainty with {
    code @Common : {
        Text            : name,
        TextArrangement : #TextOnly
    }    @title :  'Uncertainty'
};



annotate service.Employees with @(Communication.Contact : {
    fn   : employeeName,
    kind   : #individual,
    email  : [{
        address : email,
        type    : #work
    }]
	
});